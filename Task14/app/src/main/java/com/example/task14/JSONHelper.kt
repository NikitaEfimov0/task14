package com.example.task14

import android.content.Context

import com.google.gson.Gson
import java.io.IOException
import java.io.InputStreamReader


class JSONHelper {
    private val FILE_NAME = "users.json"

    fun exportToJSON(context: Context, dataList: ArrayList<Customer>): Boolean {
        val gson = Gson()
        val dataItems = DataItems()
        dataItems.users = dataList
        val jsonString = gson.toJson(dataItems)
        try {
            context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE).use { fileOutputStream ->
                fileOutputStream.write(jsonString.toByteArray())
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    fun importFromJSON(context: Context): ArrayList<Customer> {
        try {
            context.openFileInput(FILE_NAME).use { fileInputStream ->
                InputStreamReader(fileInputStream).use { streamReader ->
                    val gson = Gson()
                    val dataItems: DataItems = gson.fromJson(streamReader, DataItems::class.java)
                    return dataItems.users
                }
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }

        return ArrayList()
    }

    private class DataItems {
        var users: ArrayList<Customer> = ArrayList()
    }
}