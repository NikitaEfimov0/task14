package com.example.task14

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.task14.databinding.FragmentProfileBinding

class Profile : Fragment() {
    lateinit var interf:OpenNew
    lateinit var binding: FragmentProfileBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setValues()
        binding.exit.setOnClickListener {
            interf.hideNavBar()
            Navigation.findNavController(binding.root).navigate(R.id.toLoginFromProfile)
        }
    }

    private fun setValues(){
        binding.LoginShow.text = UserData.Login
        binding.NameShow.text = UserData.Name
        binding.PasswordShow.text = UserData.Password
        binding.Sex.text = UserData.Sex
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OpenNew){
            interf = context
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = Profile()
    }
}