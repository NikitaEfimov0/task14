package com.example.task14

interface OpenNew {
    fun checkData():Boolean
    fun saveNewData()
    fun exit()
    fun back()
    fun hideNavBar()
}