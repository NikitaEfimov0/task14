package com.example.task14

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import com.example.task14.databinding.ActivityMainBinding
import com.google.firebase.firestore.auth.User


class MainActivity : AppCompatActivity(), OpenNew {
    var currentItem:Int = R.id.home
    var jFile:String = ""
    lateinit var binding:ActivityMainBinding
    lateinit var sharedPtr:SharedPreferences
    private lateinit var users: ArrayList<Customer>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        users = ArrayList()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bottomNavigationView.visibility = View.INVISIBLE;
        sharedPtr = getSharedPreferences("Data", MODE_PRIVATE)
        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    if (currentItem == R.id.web) {
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toHomeFromWeb)
                        currentItem = R.id.home
                    }
                    else if (currentItem == R.id.profile) {
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toHomeFromProfile)
                        currentItem = R.id.home
                    }
                    else
                        Navigation.findNavController(binding.fragmentContainerView).navigate(R.id.selfHome)

                }
                R.id.web-> {
                    if (currentItem == R.id.home) {
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toWeb)
                        currentItem = R.id.web
                    }
                    else if (currentItem == R.id.profile) {
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toWebFromProfile)
                        currentItem = R.id.web
                    }
                    else
                        Navigation.findNavController(binding.fragmentContainerView).navigate(R.id.selfWeb)

                }
                R.id.UserProfile->{
                    if(currentItem == R.id.web){
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toProfileFromWeb)
                        currentItem = R.id.profile
                    }
                    else if(currentItem == R.id.home){
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toProfile)
                        currentItem = R.id.profile
                    }
                    else{
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.selfProfile)
                        currentItem = R.id.profile
                    }
                }

            }
            true
        }
    }

    override fun back(){
        Navigation.findNavController(binding.fragmentContainerView)
            .navigate(R.id.toHomeFromWeb)
        currentItem = R.id.home
    }

    override fun saveNewData(){
        open(binding.root)
        users.add(Customer(UserData.Id, UserData.Login, UserData.Password, UserData.Name, UserData.Sex))
        save(binding.root)
    }

    override fun checkData():Boolean{
        open(binding.root)
        for(x in users){
            if(x.Id == UserData.Id && x.Password == UserData.Password && x.Login == UserData.Login) {
                UserData.Name = x.Name
                UserData.Sex= x.Sex
                binding.bottomNavigationView.visibility = View.VISIBLE
                return true
            }
        }
        return false

//        sharedPtr.getString(UserData.Login, null) == UserData.Login && sharedPtr.getString(UserData.Password, null) == UserData.Password
    }


    fun open(view: View?) {
        val jH = JSONHelper()
        users = jH.importFromJSON(binding.root.context)
        if (!users.isEmpty()) {
            Toast.makeText(this, "Данные восстановлены", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Не удалось открыть данные", Toast.LENGTH_LONG).show()
        }
    }

    fun save(view: View?) {
        val jH = JSONHelper()
        val result: Boolean = jH.exportToJSON(binding.root.context, users)
        if (result) {
            Toast.makeText(this, "Данные сохранены", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Не удалось сохранить данные", Toast.LENGTH_LONG).show()
        }
    }

    override fun hideNavBar() {
        binding.bottomNavigationView.visibility = View.INVISIBLE
    }

    fun setValue(){
        for(x in users){
            if(x.Id == UserData.Id){
                UserData.Sex = x.Sex
                UserData.Name = x.Name
            }
        }
    }



    override fun exit(){
        finishAffinity()
    }
}